import React, { useContext, useState } from 'react';
import { useSelector } from 'react-redux';

import { SocketContext } from '../../context/SocketProvider';
import { State } from '../../state/reducers';
import Search from './Search';
import UserInfo from './UserInfo';
import ChatsContainer from './ChatsContainer';
import { ChatContext } from '../../context/ChatContext';

interface SideBarProps { };

const SideBar = ({ }: SideBarProps) => {
    const [search, setSearch] = useState("");
    const state = useSelector((state: State) => state);
    const { socket } = useContext(SocketContext);
    const { mobileMenu } = useContext(ChatContext);

    return (
        <div className={mobileMenu ? 'side-container expended' : 'side-container'}>
            <UserInfo userInfo={state?.user} />
            <Search setSearch={setSearch} />
            <ChatsContainer search={search} socket={socket} />
        </div>
    );
};

export default SideBar;
