import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { Socket } from "socket.io-client";

import { State } from '../../state/reducers';
import { User } from '../../types/User';
import UserCard from './UserCard';

interface ChatsProps {
    socket: Socket;
    search: string;
}

const ChatsContainer = ({ socket, search }: ChatsProps) => {
    const [users, setUsers] = useState<User[]>([]);
    const state = useSelector((state: State) => state.user);
    const usersSorted = users.sort((a, b) => b.socketID.length - a.socketID.length);

    useEffect(() => {
        socket.on('new_user_response', (data: User[]) => {
            setUsers(data);
        });
    }, [socket, users]);

    return (
        <div>
            {
                usersSorted?.map((user: User) => {
                    if (user.name === state.name) return;
                    if (search !== "" && user.name.toLowerCase().includes(search.toLowerCase())) {

                        return (
                            <UserCard userInfo={user} />
                        );
                    } else if (search === "") {

                        return (
                            <UserCard userInfo={user} />
                        );
                    }
                })
            }
        </div>
    );
};

export default ChatsContainer;
