import React from 'react';

import { User } from '../../types/User';

interface UserProps {
    userInfo: User;
}

const UserInfo = ({ userInfo }: UserProps) => {
    return (
        <div className='user-info-container'>
            <div className='avatar change-avatar'>
                <img src={userInfo.avatar} alt="" />
            </div>
            <p>{userInfo.name}</p>
        </div>
    );
};

export default UserInfo;
