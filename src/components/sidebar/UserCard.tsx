import React, { useContext } from 'react';
import axios from 'axios';
import { useSelector } from 'react-redux';

import { ChatContext } from '../../context/ChatContext';
import { User } from '../../types/User';
import { State } from '../../state/reducers';

interface UserCardProps {
    userInfo: User;
};

const UserCard = ({ userInfo }: UserCardProps) => {
    const { chatWith, setChatWith, setMessages, setMobileMenu } = useContext(ChatContext);
    const state = useSelector((state: State) => state.user);

    const openChatRoomWithUser = async () => {
        const chatParticipants: User[] = [userInfo, state];

        setChatWith(chatParticipants);
        setMobileMenu(false);

        try {
            const payload = await axios.get(process.env.REACT_APP_API_URL + `/messages_get?sender=${state.name}&reciver=${userInfo.name}`);
            setMessages(payload.data);
        } catch (error) {
            console.log(error);
        }
    };

    return (
        <div className={chatWith.includes(userInfo) ? 'chat-users active-chat-user' : 'chat-users'} onClick={() => openChatRoomWithUser()}>
            <div className='chat-info'>
                <div className='avatar'>
                    <img src={userInfo?.avatar} alt="user-avatar" />
                    <div className={userInfo.socketID === "0" ? 'online-circle' : 'online-circle online-user'}></div>
                </div>
                <div className={userInfo.socketID === "0" ? 'user-name' : 'user-name user-name-online'}>
                    <p>{userInfo.name}</p>
                </div>
            </div>
        </div>
    );
};

export default UserCard;
