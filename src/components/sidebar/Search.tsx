import React, { SetStateAction } from 'react';

interface SearchProps {
    setSearch: React.Dispatch<SetStateAction<string>>;
};

const Search = ({ setSearch }: SearchProps) => {
    return (
        <div className='search-container'>
            <input type="text" placeholder='Search for chats...' onChange={(e: React.ChangeEvent<HTMLInputElement>) => setSearch(e.currentTarget.value)} />
        </div>
    );
};

export default Search;
