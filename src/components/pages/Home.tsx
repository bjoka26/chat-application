import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { bindActionCreators } from '@reduxjs/toolkit';
import { useNavigate } from 'react-router-dom';
import { Socket } from "socket.io-client";

import actionCreators from '../../state/action-creators/actionCreators';
import Chat from '../chat/Chat';
import SideBar from '../sidebar/SideBar';
import { ChatContext } from '../../context/ChatContext';
import { User } from '../../types/User';
import { Message } from '../../types/Chat';

interface HomeProps {
    socket: Socket;
};

const Home = ({ socket }: HomeProps) => {
    const [messages, setMessages] = useState<Message[]>([]);
    const [chatWith, setChatWith] = useState<User[]>([]);
    const [mobileMenu, setMobileMenu] = useState(false);
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const { loginUser } = bindActionCreators(actionCreators, dispatch);

    const authCheck = async () => {
        const userDataFromStorage = localStorage.getItem('user');

        if (userDataFromStorage == undefined) {
            navigate("/auth");
        } else {
            const parsedUserData: User = JSON.parse(userDataFromStorage);
            socket.on("connected", () => {
                const data: User = {
                    name: parsedUserData.name,
                    avatar: parsedUserData.avatar,
                    socketID: socket.id
                };
                socket.emit('new_user', data);
                loginUser(data);
            });
        }
    };

    useEffect(() => {
        authCheck();
    }, []);

    return (
        <ChatContext.Provider value={{ chatWith, setChatWith, messages, setMessages, setMobileMenu, mobileMenu }}>
            <div className='home'>
                <SideBar />
                <Chat />
            </div>
        </ChatContext.Provider>
    );
};

export default Home;
