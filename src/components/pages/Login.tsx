import React, { useContext, useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { bindActionCreators } from 'redux';
import { useNavigate } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import actionCreators from '../../state/action-creators/actionCreators';
import AppButton from '../buttons/AppButton';
import { SocketContext } from '../../context/SocketProvider';
import { User } from '../../types/User';
import AvatarSelection from '../AvatarSelection';

interface LoginProps {
};

const Login = ({ }: LoginProps) => {
    const navigate = useNavigate();
    const [nickname, setNickname] = useState('');
    const [loading, setLoading] = useState(true);
    const [avatar, setAvatar] = useState('');
    const dispatch = useDispatch();
    const { loginUser } = bindActionCreators(actionCreators, dispatch);
    const { socket } = useContext(SocketContext);

    socket.on("connected", () => {
        setLoading(false);
    });

    const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        if (nickname === "") return toast.error("Please enter your username!", {
            toastId: 'username'
        });
        if (avatar === "") return toast.error("Please select your avatar!", {
            toastId: 'avatar'
        });

        const data: User = { name: nickname, avatar: avatar, socketID: socket.id };

        socket.emit('new_user', data);
        loginUser(data);
        navigate("/");
    };

    const authCheck = () => {
        if (localStorage.getItem('user')) {
            navigate("/");
        }

        if (socket) {
            setLoading(false);
            return;
        }
    };

    useEffect(() => {
        authCheck();
    }, []);

    return (
        <div className='login-container'>
            {!loading && <form className='login-form' onSubmit={handleSubmit}>
                <h1>Welecome to the App</h1>
                <input type="text" placeholder='Nickname' onChange={(e) => setNickname(e.currentTarget.value.trim())} />
                <AvatarSelection avatar={avatar} setAvatar={setAvatar} />
                <AppButton><p>Continue</p></AppButton>
                <ToastContainer limit={5} />
            </form>}
        </div>
    );
};

export default Login;
