import React from 'react';

interface ButtonProps {
	children: React.ReactNode;
	onPress?: (e: React.MouseEvent<HTMLButtonElement>) => void;
}

const AppButton = ({ children, onPress }: ButtonProps) => {
	return (
		<button className='app-button' onClick={onPress}>{children}</button>
	);
};

export default AppButton;
