import React, { SetStateAction, useContext, useState } from 'react';
import { useSelector } from 'react-redux';
import { FaPaperPlane } from 'react-icons/fa';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import { ChatContext } from '../../context/ChatContext';
import { SocketContext } from '../../context/SocketProvider';
import { State } from '../../state/reducers';
import { Message } from '../../types/Chat';

type Props = {
    setMessages: React.Dispatch<SetStateAction<Message[]>>;
};

const ChatFooter = ({ setMessages }: Props) => {
    const [error, setError] = useState(false);
    const [currentMessage, setCurrentMessage] = useState("");
    const state = useSelector((state: State) => state.user);
    const { chatWith } = useContext(ChatContext);
    const { socket } = useContext(SocketContext);

    const sendMesssage = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        if (currentMessage.length > 1200) {
            setError(true);
            return toast.error("Message is way too big", {
                toastId: 'avatar'
            });
        }
        if (currentMessage.trim() === "") {
            setError(true);

            return;
        }
        const messageData: Message = {
            sender: state.name,
            conversationParticipants: chatWith,
            message: currentMessage,
            date: Date.now()
        };

        socket.emit("send_message", messageData);
        setMessages((prev) => [...prev, messageData]);
        setCurrentMessage("");
    };

    return (
        <form className='form-messages' onSubmit={sendMesssage}>
            <input type="text" placeholder='Message...' value={currentMessage} onChange={(e) => setCurrentMessage(e.target.value)} />
            <button
                className={error ? 'form-button shake' : 'form-button'}
                onAnimationEnd={() => setError(false)}
            >
                <FaPaperPlane />
            </button>
            <ToastContainer limit={5} />
        </form>
    );
};

export default ChatFooter;
