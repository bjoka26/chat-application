import React, { useContext, useEffect, useState } from 'react';
import { useSelector } from 'react-redux';

import { ChatContext } from '../../context/ChatContext';
import { SocketContext } from '../../context/SocketProvider';
import { State } from '../../state/reducers';
import { Message } from '../../types/Chat';
import ChatBody from './ChatBody';
import ChatFooter from './ChatFooter';
import ChatHeader from './ChatHeader';
import ChatNotSelected from './ChatNotSelected';

interface ChatBodyProps {
};

const Chat = ({ }: ChatBodyProps) => {
    const { socket } = useContext(SocketContext);
    const state = useSelector((state: State) => state.user);
    const { messages, setMessages, chatWith, setMobileMenu, mobileMenu } = useContext(ChatContext);
    const [arrivalMessage, setArrivalMessage] = useState<Message | null>();

    const socketMessageReciver = (): void => {
        socket.on("recive_message", (message: Message) => {
            setArrivalMessage(message);
        });
    };

    useEffect(() => {
        arrivalMessage &&
            chatWith.some(e => e.name === arrivalMessage.conversationParticipants[1].name && e.name === state.name) &&
            setMessages((prev) => [...prev, arrivalMessage]);
        setArrivalMessage(null);
    }, [arrivalMessage, chatWith]);

    useEffect(() => {
        socketMessageReciver();

        return () => {
            socket.off('recive_message');
        };
    }, []);

    return (
        <div className='chat-container'>
            <ChatHeader chatWith={chatWith} setMobileMenu={setMobileMenu} mobileMenu={mobileMenu} />
            {chatWith.length != 0 ? <>
                <ChatBody messages={messages} />
                <ChatFooter setMessages={setMessages} />
            </>
                :
                <ChatNotSelected />
            }
        </div>
    );
};

export default Chat;
