import React, { useEffect, useRef } from 'react';
import { useSelector } from 'react-redux';
import { format } from 'timeago.js';

import bgImg from '../../assets/ice-cream.png';
import { State } from '../../state/reducers';
import { Message } from '../../types/Chat';

interface ChatBodyProps {
    messages: Message[];
};

const ChatBody = ({ messages }: ChatBodyProps) => {
    const state = useSelector((state: State) => state.user);
    const lastMessageRef = useRef<HTMLDivElement>(null);

    useEffect(() => {
        lastMessageRef.current?.scrollIntoView({ behavior: 'smooth' });
    }, [messages]);

    return (
        <div className='message-container'>
            <div className='bg-img'>
                <img src={bgImg} alt="" />
            </div>
            <div className='messages'>
                {messages?.map((message: Message) =>
                    message.sender === state.name ? (
                        <div className="message sender" key={message.id}>
                            <div className="message-sender">
                                <p>{message.message}</p>
                            </div>
                            <p className='date'>{format(message.date)}</p>
                        </div>
                    ) : (
                        <div className="message reciver" key={message.id}>
                            <h4>{message.sender}</h4>
                            <div className="message-reciver">
                                <p>{message.message}</p>
                            </div>
                            <p className='date'>{format(message.date)}</p>
                        </div>
                    )
                )}
                <div ref={lastMessageRef}></div>
            </div>
        </div>
    );
};

export default ChatBody;
