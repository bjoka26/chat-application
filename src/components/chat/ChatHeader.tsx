import React, { SetStateAction, useContext } from 'react';
import { FaBars } from 'react-icons/fa';
import { MdClose } from 'react-icons/md';
import { MdLogout } from 'react-icons/md';
import { useDispatch } from 'react-redux';
import { bindActionCreators } from 'redux';
import { useNavigate } from 'react-router-dom';

import actionCreators from '../../state/action-creators/actionCreators';
import { User } from '../../types/User';
import AppButton from '../buttons/AppButton';
import { SocketContext } from '../../context/SocketProvider';

interface ChatHeaderProps {
    chatWith: User[];
    setMobileMenu: React.Dispatch<SetStateAction<boolean>>;
    mobileMenu: boolean;
};

const ChatHeader = ({ chatWith, setMobileMenu, mobileMenu }: ChatHeaderProps) => {
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const { logOutUser } = bindActionCreators(actionCreators, dispatch);

    const logOut = () => {
        navigate('/auth');
        logOutUser();
    };

    return (
        <div className='chat-info-messages'>
            <i className={mobileMenu ? 'mobile-menu opened' : 'mobile-menu'} onClick={() => setMobileMenu(!mobileMenu)}>{mobileMenu ? <MdClose /> : <FaBars />}</i>
            <div className='user'>
                {
                    chatWith.length !== 0
                    &&
                    <div className='avatar'>
                        <img src={chatWith[0].avatar} alt="user-avatar" />
                    </div>
                }
                <div className='name-container'>
                    <p style={{ fontSize: chatWith.length !== 0 ? '1.2rem' : '0.8rem' }}>{chatWith.length !== 0 ? chatWith[0].name : "Please select the user you want to chat with"}</p>
                </div>
            </div>
            <AppButton onPress={logOut}>
                <MdLogout />
            </AppButton>
        </div>
    );
};

export default ChatHeader;
