import React from 'react';

interface ChatNotSelectedProps { };

const ChatNotSelected = ({ }: ChatNotSelectedProps) => {
    return (
        <div className="not-selected">
            <p>Chat not selected</p>
        </div>
    );
};

export default ChatNotSelected;
