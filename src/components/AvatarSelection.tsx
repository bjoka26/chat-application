import React, { SetStateAction } from 'react';

interface AvatarProps {
    setAvatar: React.Dispatch<SetStateAction<string>>;
    avatar: string;
};

const AvatarSelection = ({ setAvatar, avatar }: AvatarProps) => {
    const avatars: string[] = [
        "https://i.imgur.com/ejXeq3T.png",
        "https://i.imgur.com/agmHzC2.png",
        "https://i.imgur.com/aDEGZS6.png",
        "https://i.imgur.com/6DzvSIr.png",
    ];

    return (
        <div className='avatar-selection'>
            {
                avatars.map((avatarImg: string) => {
                    return (
                        <div className={avatarImg === avatar ? 'avatar-select selected-avatar' : 'avatar-select'} onClick={() => setAvatar(avatarImg)}>
                            <img src={avatarImg} alt="" />
                        </div>
                    );
                })
            }
        </div>
    );
};

export default AvatarSelection;