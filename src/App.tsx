import React from 'react';
import './scss/app.css';
import {
	BrowserRouter,
	Routes,
	Route,
} from "react-router-dom";
import { io, Socket } from "socket.io-client";

import Login from './components/pages/Login';
import Home from './components/pages/Home';
import { SocketContext } from './context/SocketProvider';

function App() {
	const socket: Socket = io('ws://161.97.129.146:4000');
	socket.emit("connected");

	return (
		<BrowserRouter>
			<SocketContext.Provider value={{ socket }}>
				<div className="App">
					<Routes>
						<Route path="/auth" element={<Login />} />
						<Route path="/" element={<Home socket={socket} />} />
					</Routes>
				</div>
			</SocketContext.Provider>
		</BrowserRouter >
	);
}

export default App;
