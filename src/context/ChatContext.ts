import React, { createContext, SetStateAction } from "react";

import { Message } from "../types/Chat";
import { User } from "../types/User";

interface ChatContext {
    chatWith: User[];
    setChatWith: React.Dispatch<SetStateAction<User[]>>;
    messages: Message[];
    setMessages: React.Dispatch<SetStateAction<Message[]>>;
    setMobileMenu: React.Dispatch<SetStateAction<boolean>>;
    mobileMenu: boolean;
};

export const ChatContext = createContext<ChatContext>({} as ChatContext);
