import React, { createContext } from "react";
import { Socket } from "socket.io-client";

interface SocketProps {
    socket: Socket;
}

export const SocketContext = createContext<SocketProps>({} as SocketProps);
