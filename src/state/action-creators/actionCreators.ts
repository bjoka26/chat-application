import React from 'react';
import { Dispatch } from 'redux';

import { ACTION } from '../action-types/actions';
import { ActionReducer } from '../action-types/actions';
import { User } from '../../types/User';

const addNewMessage = (message: string) => {
    return (dispatch: Dispatch<ActionReducer>) => {
        dispatch({
            type: ACTION.SEND_MESSAGE,
            payload: message
        });
    };
};

const loginUser = (userInfo: User) => {
    return (dispatch: Dispatch<ActionReducer>) => {
        dispatch({
            type: ACTION.LOGIN,
            payload: userInfo
        });
    };
};

const logOutUser = () => {
    return (dispatch: Dispatch<ActionReducer>) => {
        dispatch({
            type: ACTION.LOGOUT,
            payload: "logout"
        });
    };
};

const addUsers = (users: User[]) => {
    return (dispatch: Dispatch<ActionReducer>) => {
        dispatch({
            type: ACTION.LOAD_USERS,
            payload: users
        });
    };
};

export default { addNewMessage, loginUser, addUsers, logOutUser };
