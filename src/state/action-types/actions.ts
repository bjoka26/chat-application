export enum ACTION {
    NEW_CHAT = "NEW_CHAT",
    LOGIN = "LOGIN",
    LOGOUT = "LOGOUT",
    SEND_MESSAGE = "SEND_MESSAGE",
    LOAD_USERS = "LOAD_USERS"
};

export type ActionReducer = {
    type: string;
    payload: any;
};
