import { User } from '../../types/User';
import { ACTION } from '../action-types/actions';
import { ActionReducer } from '../action-types/actions';

const reducer = (state: User = {
    name: '',
    avatar: '',
    socketID: ''
}, action: ActionReducer) => {
    switch (action.type) {
        case ACTION.LOGIN:
            localStorage.setItem('user', JSON.stringify(action.payload));
            return state = action.payload;
        case ACTION.LOGOUT:
            localStorage.removeItem('user');
            return state = action.payload;

        default:
            return state;
    }
};

export default reducer;
