import { Chat, Message } from '../../types/Chat';
import { User } from '../../types/User';
import { ACTION } from '../action-types/actions';
import { ActionReducer } from '../action-types/actions';

const reducer = (state: Chat[] | Message[] | User[] = [], action: ActionReducer) => {
    switch (action.type) {
        case ACTION.NEW_CHAT:
            return [...state, action.payload];
        case ACTION.SEND_MESSAGE:
            return [...state, action.payload];
        case ACTION.LOAD_USERS:
            return [...state, action.payload];

        default:
            return state;
    }
};

export default reducer;
