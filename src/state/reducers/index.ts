import { combineReducers } from "redux";

import chatRecuder from '../reducers/chatReducer';
import userReducer from '../reducers/userReducer';

const reducers = combineReducers({
    chat: chatRecuder,
    user: userReducer
});

export default reducers;

export type State = ReturnType<typeof reducers>;

