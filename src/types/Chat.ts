import { ACTION } from '../state/action-types/actions';
import { User } from './User';

export type Chat = {
    chatWithUser: User;
    messages: Message[];
    lastMessage: string;
    date: string;
};

export type ChatAction = {
    type: ACTION;
    chat: Chat;
};

export type Message = {
    id?: number;
    sender: string;
    conversationParticipants: User[];
    message: string;
    date: number;
};
