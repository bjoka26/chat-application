export type User = {
    name: string;
    avatar: string;
    socketID: string;
};
